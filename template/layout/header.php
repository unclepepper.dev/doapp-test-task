<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php if(!empty($title)) echo $title; ?></title>
    <script
        src="https://code.jquery.com/jquery-3.6.1.js"
        integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
        crossorigin="anonymous">
    </script>
    <style>
        .wrap{
            margin: auto;
            width: 70%;
            padding:  1% 0;

        }
        .nav{
            width: 90%;;
            height: 60px;
            background-color: blue;
            margin: auto;
            display: flex;
            justify-content: space-between;
            text-align: center;
            padding:0 10% 0 10%;
            box-shadow: 0 0  5px #000;
            border-radius: 3px;
        }
        .content{
            width: 90%;
            padding:1% 10% 2% 10%;
            box-shadow: 0 0 10px aqua;
            margin: 1% auto;
        }
        ul{
            list-style-type: none;
            justify-content: end;
        }
        .nav-ul{
            display: flex;
            align-items: center;
        }
        li{
            margin-left: 10px;
        }
        a{
            text-decoration: none;
            color: white;
        }
        h2{
            color: white;
            align-items: center;
        }
        .users{
            margin-top: 50px;
        }
    </style>
</head>
<body>
<div class="wrap">
    <div class="nav">
        <h2>DoApp php test</h2>
        <ul class="nav-ul">


            <?php if(isset($_SESSION['account']['id'])): ?>
                <?php if ($this->route['action'] != 'index'): ?>
                    <li><a href="/">Пользователи</a></li>
                <?php endif; ?>
                <?php if ($this->route['action'] != 'register'): ?>
                    <li><a href="/account/register">Зарегистрировать</a></li>
                <?php endif; ?>
                <li><a href="/account/logout"><b><?= $_SESSION['account']['firstName'] ?></b><span> &#8658;</span></a></li>
            <?php endif; ?>


        </ul>
    </div>
    <div class="content">
