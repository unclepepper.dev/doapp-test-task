<?php

return [
    '' => [
        'controller' => 'index',
        'action' => 'index',
    ],
    'account/login' => [
      'controller' => 'account',
      'action' => 'login',
      ],
    'account/register' => [
        'controller' => 'account',
        'action' => 'register',
    ],
    'account/logout' => [
        'controller' => 'account',
        'action' => 'logout',
    ],
];