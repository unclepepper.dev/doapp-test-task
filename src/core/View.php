<?php

declare(strict_types=1);

namespace src\core;

use JetBrains\PhpStorm\NoReturn;

class View
{
    public string $path;
    public array $route;
    public string $header = 'header';
    public string $footer = 'footer';

    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'] . '/' . $route['action'];
    }

    public function render($title, $args = []): void
    {
        $filePath = dirname(__DIR__) . '/../template/' . $this->path . '.php';
        $headerPath = dirname(__DIR__) . '/../template/layout/' . $this->header . '.php';
        $footerPath = dirname(__DIR__) . '/../template/layout/' . $this->footer . '.php';

        extract($args);
        if(file_exists($filePath)){
            require $headerPath;
            require $filePath;
            require $footerPath;
        } else {
            self::error(404);
        }
    }

    #[NoReturn]
    public static function error($code): void
    {
        http_response_code($code);
        $errorPath = dirname(__DIR__) . '/../../template/errors/' . $code . '.php';
        if(file_exists($errorPath)){
            require $errorPath;
            exit;
        }
    }

    #[NoReturn]
    public function redirect(string $url): void
    {
        header('location: ' .$url);
        exit;
    }
}