<h3>Создание пользователя</h3>
<form action="/account/register" method="post">
    <p><label for="firstName">Имя</label></p>
    <p><input type="text" id="firstName" name="firstName" required></p>
    <p><label for="lastName">Фамилия</label></p>
    <p><input type="text" id="lastName" name="lastName" ></p>
    <p><label for="age">Возраст</label></p>
    <p><input type="number" id="age" name="age" required></p>
    <p><label for="email">Email</label></p>
    <p><input type="email" id="email" name="email" required></p>
    <p><label for="password">Password</label></p>
    <p><input type="password" id="password" name="password" required></p>
    <button type="submit">Зарегистрировать</button>
</form>
<?php
