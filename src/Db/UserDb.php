<?php

declare(strict_types=1);

namespace src\Db;

use src\models\User;

class UserDb
{

    private function getPath(): string
    {
        $title = basename(str_replace('\\', '/', __CLASS__));
        return dirname(__DIR__) . '/../db/' . lcfirst($title) . '.json';
    }

    public function save(?User $user): void
    {
        $file = $this->getPath();
        $jsonArray= [];

        if (file_exists($file)){
            $json = file_get_contents($file);
            $jsonArray = json_decode($json, true);
        }

        if ($user){
            $jsonArray[] = [
               'id' => $user->getId(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
                'age' => $user->getAge(),
                'email' => $user->getEmail(),
                'password' => $user->getPassword(),
                ];
            file_put_contents($file, json_encode($jsonArray, JSON_FORCE_OBJECT));
        }
    }

    public function getAll(): mixed
    {
        $file = $this->getPath();
        if (file_exists($file)){
            $json = file_get_contents($this->getPath());
            return json_decode($json, true);
        }else {
            return [];
        }
    }

    public function getUserId(?string $email): ?string
    {
        $users = $this->getAll();
        $result = null;

            foreach ($users as $user){
                if($user['email'] === $email){
                    $result = $user['id'];
                }
            }
        return $result;
    }

    public function getUserName(?string $email): ?string
    {
        $users = $this->getAll();
        $result = null;

        foreach ($users as $user){
            if($user['email'] === $email){
                $result = $user['firstName'];
            }
        }
        return $result;
    }

}