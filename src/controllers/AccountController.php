<?php

declare(strict_types=1);

namespace src\controllers;

use JetBrains\PhpStorm\NoReturn;
use src\core\Controller;
use src\service\Location;
use src\service\StatusMessage;
use src\service\RegistrationService;
use src\Db\UserDb;

class AccountController extends Controller
{
    private StatusMessage $message;
    private Location $loc;
    private RegistrationService $register;
    private UserDb $db;

    public function __construct($route)
    {
        parent::__construct($route);
        $this->message = new StatusMessage();
        $this->loc = new Location();
        $this->register = new RegistrationService();
        $this->db = new UserDb();
    }

    public function loginAction(): void
    {

        if (!empty($_POST)) {
            if($this->register->validate(['email', 'password'], $_POST)){
                if(!$this->register->auth($_POST['email'], $_POST['password'])){
                    $this->message->error('error', 'Неправильный логин или пароль');
                }

                $_SESSION['account']['id'] = $this->db->getUserId($_POST['email']);
                $_SESSION['account']['firstName'] = $this->db->getUserName($_POST['email']);

                $this->loc->location('/');
            }
        }

        $this->view->render('Вход в систему');
    }

    public function registerAction(): void
    {
        if(!isset($_SESSION['account']['id'])){
            $this->view->redirect('/account/login');
        }

        if (!empty($_POST)) {
            if($this->register->validate(['email', 'password'], $_POST)){
                if($this->register->checkEmailExist($_POST['email'])){
                    $this->message->error('error', sprintf('Пользователь с имейлом %s уже зарегистрирован', $_POST['email']));
                }

                $this->register->userRegister($_POST);
                $this->message->error('error', sprintf('Пользователь  %s успешно зарегистрирован!', $_POST['email']));
            }
        }

        $this->view->render('Регистрация пользователя');
    }

    #[NoReturn] public function logoutAction(): void
    {
        unset($_SESSION['account']['id']);
        $this->view->redirect('/account/login');
    }

}