FROM php:8.0.0-fpm-alpine as php

RUN set -xe \
        && apk add --no-cache \
           shadow \
           libzip-dev \
           libintl \
           icu \
           icu-dev \
           curl \
           libmcrypt \
           libmcrypt-dev \
           libxml2-dev \
           freetype \
           freetype-dev \
           libpng \
           libpng-dev \
           libjpeg-turbo \
           libjpeg-turbo-dev \
           postgresql-dev \
           pcre-dev \
           git \
           g++ \
           make \
           autoconf \
           openssh \
           util-linux-dev \
           libuuid \
           sqlite-dev \
           libxslt-dev

RUN docker-php-ext-install \
    zip \
    iconv \
    soap \
    sockets \
    intl \
    pdo_mysql \
    pdo_pgsql \
    exif \
    pcntl \
    xsl



ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1

ARG UID
ARG GID
ENV TARGET_UID ${UID:-1000}
ENV TARGET_GID ${GID:-1000}


RUN usermod -u ${TARGET_UID} www-data && groupmod -g ${TARGET_UID} www-data
RUN mkdir -p /var/www/app/vendor && mkdir -p /var/www/app/var/cache && chown -R www-data:www-data /var/www/app
RUN mkdir -p /var/sf/vendor && mkdir -p /var/sf/var && chown -R www-data:www-data /var/sf

WORKDIR /var/www/app

USER ${TARGET_UID}:${TARGET_GID}
