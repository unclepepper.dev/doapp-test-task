<?php

declare(strict_types=1);

namespace src\service;

use JetBrains\PhpStorm\NoReturn;

class StatusMessage
{
    #[NoReturn] public function error(string $status, ?string $message = ''): void
    {
        exit(json_encode([
            'status' => $status,
            'message' => $message
        ]));
    }

}