<?php

declare(strict_types=1);

namespace src\controllers;

use src\core\Controller;
use src\Db\UserDb;

class IndexController extends Controller
{
    private UserDb $db;

    public function __construct($route)
    {
        parent::__construct($route);
        $this->db = new UserDb();
    }

    public function indexAction()
    {
        if(!isset($_SESSION['account']['id'])){
            $this->view->redirect('/account/login');
        }

        if(!$this->db->getAll()){
            $this->view->redirect('account/register');
        }

        $param = $this->db->getAll();

        $this->view->render('Списки пользователей', [
          'param' => $param,
        ]);
    }
}