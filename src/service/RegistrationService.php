<?php

declare(strict_types=1);

namespace src\service;

use src\Db\UserDb;
use src\models\User;

class RegistrationService
{
    private StatusMessage $message;
    private UserDb $db;

    public function __construct()
    {
        $this->message = new StatusMessage();
        $this->db  = new UserDb();

    }

    public function userRegister(array $post): void
    {
        $firstName = htmlspecialchars(trim($post['firstName']));
        $lastName = htmlspecialchars(trim($post['lastName']));
        $age = (int)htmlspecialchars(trim($post['age'])) ?? null;
        $email = htmlspecialchars(trim($post['email']));
        $password = password_hash(htmlspecialchars(trim($post['password'])), PASSWORD_BCRYPT);

        $user = new User();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setAge($age);
        $user->setEmail($email);
        $user->setPassword($password);
        $this->db->save($user);
    }

        public function auth(string $email, string $password): bool
    {
        $users = $this->db->getAll();
        $res = true;
        foreach ($users as $user) {
            if ($user['email'] === $email) {
                $hash = $user['password'];
                $res = (password_verify($password, $hash));
            }
        }
        return $res;
    }


    public function validate(array $input, array $post): bool
    {
        $rules = [
            'email' => [
                'pattern' => '#^([a-z0-9_.-]{1,20}+)@([a-z0-9_.-]+)\.([a-z\.]{2,10})$#',
                'message' => 'Введите корректный email',
            ],
            'password' => [
                'pattern' => '#^[a-z0-9]{6,10}$#',
                'message' => 'Введите корректный пароль (только латинские буквы и цифры от 6 до 10 символов)',
            ],
        ];

        foreach ($input as $item){
            if(!isset($post[$item]) || !preg_match($rules[$item]['pattern'], $post[$item])){
                $this->message->error('error', $rules[$item]['message']);
            }
        }
        return true;
    }

    public function checkEmailExist($email): bool
    {
        $users = $this->db->getAll();

        return (bool)array_filter($users, fn($u) => $u['email'] === $email) ?? false;

    }

}