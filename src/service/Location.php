<?php

declare(strict_types=1);

namespace src\service;

use JetBrains\PhpStorm\NoReturn;

class Location
{
    #[NoReturn] public function location(string $url): void
    {
        exit(json_encode([
            'url' => $url,
        ]));
    }
}